package project.carryit.Controler;

import android.support.constraint.ConstraintLayout;
import android.text.Editable;

import project.carryit.Models.Car;
import project.carryit.Models.Cart;
import project.carryit.Models.Moto;
import project.carryit.Models.Van;
import project.carryit.R;
import project.carryit.View.AtarefarFragment;

import android.text.TextWatcher;

public class TextVerify implements TextWatcher{

    Car objectcar;
    Van objectvan;
    Moto objectmoto;
    Cart objectcart;
    AtarefarFragment frag;
    AtarefarControler controler;
    ConstraintLayout van, moto, carreta, carro;

    public TextVerify(ConstraintLayout van, ConstraintLayout moto, ConstraintLayout carreta, ConstraintLayout carro, AtarefarControler controler, Car objectcar, Van objectvan, Cart objectcart, Moto objectmoto, AtarefarFragment frag) {

        this.van = van;
        this.moto = moto;
        this.carreta = carreta;
        this.carro = carro;
        this.controler = controler;
        this.objectcar = objectcar;
        this.objectmoto = objectmoto;
        this.objectcart = objectcart;
        this.objectvan = objectvan;
        this.frag = frag;

    }

    @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            van.setBackgroundResource(R.drawable.shape_background_second);
            moto.setBackgroundResource(R.drawable.shape_background_second);
            carro.setBackgroundResource(R.drawable.shape_background_second);
            carreta.setBackgroundResource(R.drawable.shape_background_second);

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {


        }

        @Override
        public void afterTextChanged(Editable s) {

            String atualityLoad = s.toString();

            if(Float.valueOf(atualityLoad) < objectcar.getMaximumLoad()){
                controler.updateQuant("Carro", this, carro);
            }
            else{
                frag.negAction(carro);
            }



            if(Float.valueOf(atualityLoad) < objectvan.getMaximumLoad()){
                controler.updateQuant("Van", this, van);
            }
            else{
                frag.negAction(van);
            }



            if(Float.valueOf(atualityLoad) < objectmoto.getMaximumLoad()){
                controler.updateQuant("Moto", this, moto);
            }
            else{
                frag.negAction(moto);
            }



            if(Float.valueOf(atualityLoad) < objectcart.getMaximumLoad()){
                controler.updateQuant("Carreta", this, carreta);
            }
            else{
                frag.negAction(carreta);
            }
        }

        public void verifyQuantity(VehicleControler user, ConstraintLayout veiculo, String vehicle){

            if( Integer.valueOf(user.getQuantidadealugado()) < Integer.valueOf(user.getQuantidadegeral() )) {
                frag.adjustInterface(veiculo, vehicle);
            }
            else {
                frag.negAction(veiculo);
            }

        }

}
