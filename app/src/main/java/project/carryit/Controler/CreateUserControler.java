package project.carryit.Controler;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class CreateUserControler {

    private FirebaseAuth mAuth;
    private DatabaseReference mDataBase;


    public CreateUserControler(){

    }

    public void updateUserInfo(String email, String name, FirebaseUser currentUser) {

        mAuth = FirebaseAuth.getInstance();
        mDataBase = FirebaseDatabase.getInstance().getReference().child("Users");

        String user_id = currentUser.getUid();
        Map newpost = new HashMap();
        newpost.put("name", name);
        newpost.put("email", email);
        newpost.put("margem_de_lucro", 10);
        Map startvalue = new HashMap();
        startvalue.put("quantidade", 1);
        startvalue.put("quantalugada", 0);
        Map startporc = new HashMap();
        mDataBase.child(user_id).setValue(newpost);
        mDataBase.child(user_id).child("Carreta").setValue(startvalue);
        mDataBase.child(user_id).child("Carro").setValue(startvalue);
        mDataBase.child(user_id).child("Moto").setValue(startvalue);
        mDataBase.child(user_id).child("Van").setValue(startvalue);

    }




}
