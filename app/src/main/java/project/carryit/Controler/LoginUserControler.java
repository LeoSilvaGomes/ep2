package project.carryit.Controler;

import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import project.carryit.View.LoginActivity;


public class LoginUserControler {

    private FirebaseAuth mAuth;
    LoginActivity loginActivity;

    public LoginUserControler(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
    }

    public void signIn(String mail, String password) {

        mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithEmailAndPassword(mail,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){

                    loginActivity.VerifySignIn("SignIn Accept");

                }
                else{

                    loginActivity.VerifySignIn(task.getException().getMessage());

                }
            }
        });

    }

}
