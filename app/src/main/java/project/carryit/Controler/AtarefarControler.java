package project.carryit.Controler;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import project.carryit.Models.Car;
import project.carryit.Models.Cart;
import project.carryit.Models.Moto;
import project.carryit.Models.User;
import project.carryit.Models.Van;

public class AtarefarControler {

    private FirebaseAuth mAuth;
    private DatabaseReference mDataBase;
    private Integer quantidadealugada;

    User user = new User();
    Van van = new Van();
    Car car =  new Car();
    Cart cart = new Cart();
    Moto moto = new Moto();


    public void addOnDataBase(final String componente, String nomeDestino, Map valoresViagem, Map valoresCusto){

        mAuth = FirebaseAuth.getInstance();
        String user_id = mAuth.getCurrentUser().getUid();
        mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);


        mDataBase.child(componente).child(nomeDestino).setValue(valoresViagem);
        valoresViagem.put("Veiculo", componente);
        mDataBase.child("Historico").child(nomeDestino).setValue(valoresViagem);
        mDataBase.child("Custo").child(nomeDestino).setValue(valoresCusto);


        if(componente.equals("Carro")){
            car.setQuantidadealugada(car.getQuantidadealugada() + 1);
            quantidadealugada = car.getQuantidadealugada();
            mDataBase.child(componente).child("quantalugada").setValue(quantidadealugada);
        }
        if(componente.equals("Van")){
            van.setQuantidadealugada(van.getQuantidadealugada() + 1);
            quantidadealugada = van.getQuantidadealugada();
            mDataBase.child(componente).child("quantalugada").setValue(quantidadealugada);
        }
        if(componente.equals("Carreta")){
            cart.setQuantidadealugada(cart.getQuantidadealugada() + 1);
            quantidadealugada = cart.getQuantidadealugada();
            mDataBase.child(componente).child("quantalugada").setValue(quantidadealugada);
        }
        if(componente.equals("Moto")){
            moto.setQuantidadealugada(moto.getQuantidadealugada() + 1);
            quantidadealugada = moto.getQuantidadealugada();
            mDataBase.child(componente).child("quantalugada").setValue(quantidadealugada);
        }



    }





    public VehicleControler updateQuant(final String vehicle, TextVerify t, ConstraintLayout veiculos) {

        mAuth = FirebaseAuth.getInstance();
        String user_id = mAuth.getCurrentUser().getUid();
        mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
        VehicleControler vehicleModel = new VehicleControler(vehicle, t, veiculos);
        mDataBase.addValueEventListener(vehicleModel);
        vehicleModel.getQuantidadegeral();

        return vehicleModel;

    }




    public void updateDataBaseName(final TextView userName) {

        mAuth = FirebaseAuth.getInstance();
        String user_id = mAuth.getCurrentUser().getUid();
        mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);

        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("name").getValue().toString();
                String email = dataSnapshot.child("email").getValue().toString();
                String quantidade = dataSnapshot.child("Van").child("quantidade").getValue().toString();
                String quantidadeAlugada = dataSnapshot.child("Van").child("quantalugada").getValue().toString();
                van.setQuantidadealugada(Integer.valueOf(quantidadeAlugada));
                van.setQuantidade(Integer.valueOf(quantidade));
                quantidade = dataSnapshot.child("Carro").child("quantidade").getValue().toString();
                quantidadeAlugada = dataSnapshot.child("Carro").child("quantalugada").getValue().toString();
                car.setQuantidadealugada(Integer.valueOf(quantidadeAlugada));
                car.setQuantidade(Integer.valueOf(quantidade));
                quantidade = dataSnapshot.child("Carreta").child("quantidade").getValue().toString();
                quantidadeAlugada = dataSnapshot.child("Carreta").child("quantalugada").getValue().toString();
                cart.setQuantidadealugada(Integer.valueOf(quantidadeAlugada));
                cart.setQuantidade(Integer.valueOf(quantidade));
                quantidade = dataSnapshot.child("Moto").child("quantidade").getValue().toString();
                quantidadeAlugada = dataSnapshot.child("Moto").child("quantalugada").getValue().toString();
                moto.setQuantidadealugada(Integer.valueOf(quantidadeAlugada));
                moto.setQuantidade(Integer.valueOf(quantidade));
                user.setPorcetagem(10);
                user.setName(name);
                user.setEmail(email);
                userName.setText(name);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



}
