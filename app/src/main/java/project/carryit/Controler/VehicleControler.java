package project.carryit.Controler;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import project.carryit.Controler.TextVerify;

public class VehicleControler implements ValueEventListener {

    final String vehicle;
    TextVerify t;
    String quantidadealugado = "";
    String quantidadegeral = "";
    ConstraintLayout veiculos;


    public String getQuantidadealugado() {
        return quantidadealugado;
    }

    public String getQuantidadegeral() {
        return quantidadegeral;
    }

    public VehicleControler(String vehicle, TextVerify t, ConstraintLayout veiculos) {
        this.vehicle = vehicle;
        this.t = t;
        this.veiculos = veiculos;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


        quantidadealugado = dataSnapshot.child(vehicle).child("quantalugada").getValue().toString();
        quantidadegeral = dataSnapshot.child(vehicle).child("quantidade").getValue().toString();
        t.verifyQuantity(this, veiculos, vehicle);

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }



}
