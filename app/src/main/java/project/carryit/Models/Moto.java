package project.carryit.Models;

public class Moto extends Vehicle {

    private int chooseFuel;

    public Moto(){
        setYield(50.0f);
        setChooseFuel(0);
        setQuantity(1);
        setFuel(1);
        setAverageSpeed(110.0f);
        setFuelPrice(getFuel());
        setMinimumYield(0.3f);
        setMaximumLoad(50.0f);
        setQuantidade(1);
        setQuantidadealugada(0);
    }

    public int getChooseFuel() {
        return chooseFuel;
    }

    public void setChooseFuel(int chooseFuel) {
        this.chooseFuel = chooseFuel;
    }

}
