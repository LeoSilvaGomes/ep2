package project.carryit.Models;

public class Car extends Vehicle {

    private int chooseFuel;

    public Car(){
        setYield(14.0f);
        setChooseFuel(0);
        setQuantity(1);
        setFuel(1);
        setAverageSpeed(100.0f);
        setFuelPrice(getFuel());
        setMinimumYield(0.0025f);
        setMaximumLoad(360.0f);
        setQuantidade(1);
        setQuantidadealugada(0);
    }


    public int getChooseFuel() {
        return chooseFuel;
    }

    public void setChooseFuel(int chooseFuel) {
        this.chooseFuel = chooseFuel;
    }

}
