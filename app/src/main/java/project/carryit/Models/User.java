package project.carryit.Models;

public class User {

    private String name;
    private String email;
    private Integer quantidadeAludada;
    private Integer quantidade;
    private Integer porcetagem;

    public User (){
        setPorcetagem(10);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getQuantidadeAludada() {
        return quantidadeAludada;
    }

    public void setQuantidadeAludada(Integer quantidadeAludada) {
        this.quantidadeAludada = quantidadeAludada;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPorcetagem() {
        return porcetagem;
    }

    public void setPorcetagem(Integer porcetagem) {
        this.porcetagem = porcetagem;
    }
}
