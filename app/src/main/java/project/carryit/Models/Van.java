package project.carryit.Models;

public class Van extends Vehicle {

    public Van(){
        setYield(10.0f);
        setFuel(3);
        setQuantity(1);
        setAverageSpeed(80.0f);
        setFuelPrice(getFuel());
        setMinimumYield(0.001f);
        setMaximumLoad(3500.0f);
        setQuantidade(1);
        setQuantidadealugada(0);
    }

    @Override
    public float getMinimumYield() {
        return super.getMinimumYield();
    }

    @Override
    public int getFuel() {
        return super.getFuel();
    }

    @Override
    public float getYield() {
        return super.getYield();
    }

    @Override
    public float getMaximumLoad() {
        return super.getMaximumLoad();
    }

    @Override
    public float getAverageSpeed() {
        return super.getAverageSpeed();
    }

    @Override
    public int getQuantity() {
        return super.getQuantity();
    }
}
