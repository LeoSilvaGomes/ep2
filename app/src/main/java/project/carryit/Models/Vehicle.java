package project.carryit.Models;

public class Vehicle {

    private int fuel;
    private int quantity;
    private float yield;
    private float fuelPrice;
    private float maximumLoad;
    private float averageSpeed;
    private float minimumYield;
    private String destinyName;
    private float distanci;
    private float tempo;
    private int quantidade;
    private int quantidadealugada;
    private float peso;

    private static final int DIESEL = 1;
    private static final int GASOLINA = 2;
    private static final int ALCOOL = 3;

    public Vehicle() {
        setFuel(0);
        setFuelPrice(getFuel());
        setAverageSpeed(1.0f);
        setMaximumLoad(1.0f);

        setYield(1.0f);
    }


    public void setFuelPrice(int fuel) {

        if(fuel == 3){
            this.fuelPrice =  3.869f;
        }
        else if(fuel == 2){
            this.fuelPrice = 4.449f;
        }
        else if(fuel == 1){
            this.fuelPrice = 3.499f;
        }
        else{
            this.fuelPrice = 1.0f;
        }

    }


    public float getMinimumYield() {
        return minimumYield;
    }

    public void setMinimumYield(float minimumYield) {
        this.minimumYield = minimumYield;
    }

    public int getFuel() {
        return fuel;
    }

    protected void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public float getYield() {
        return yield;
    }

    protected void setYield(float yield) {
        this.yield = yield;
    }

    public float getMaximumLoad() {
        return maximumLoad;
    }

    protected void setMaximumLoad(float maximumLoad) {
        this.maximumLoad = maximumLoad;
    }

    public float getAverageSpeed() {
        return averageSpeed;
    }

    protected void setAverageSpeed(float averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public String getDestinyName() {
        return destinyName;
    }

    public float getDistanci() {
        return distanci;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getQuantidadealugada() {
        return quantidadealugada;
    }

    public void setQuantidadealugada(int quantidadealugada) {
        this.quantidadealugada = quantidadealugada;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getTempo() {
        return tempo;
    }

    public void setTempo(float fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public void setDestinyName(String destinyName) {
        this.destinyName = destinyName;
    }

    public void setDistanci(float distanci) {
        this.distanci = distanci;
    }

    public float LessTime(float distance){
        return distance/averageSpeed;
    }

    public float LeastCost(float distance, float fullLoad){
        return  (distance/(yield - fullLoad * minimumYield)) * fuelPrice;
    }




    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
