package project.carryit.Models;

public class Cart extends Vehicle {

    public Cart(){
        setFuel(3);
        setYield(8.0f);
        setQuantity(1);
        setAverageSpeed(60.0f);
        setFuelPrice(getFuel());
        setMinimumYield(0.0002f);
        setMaximumLoad(30000.0f);
        setQuantidade(1);
        setQuantidadealugada(0);
    }
}
