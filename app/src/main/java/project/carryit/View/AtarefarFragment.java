package project.carryit.View;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import project.carryit.Models.Car;
import project.carryit.Models.Cart;
import project.carryit.Controler.AtarefarControler;
import project.carryit.Models.Moto;
import project.carryit.Controler.TextVerify;
import project.carryit.Models.User;
import project.carryit.R;
import project.carryit.Models.Van;


public class AtarefarFragment extends Fragment {


    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;
    private TextView dataentreg, datachegada, userName, maisrapidocarro, maisrapidovan, maisrapidomoto, maisrapidocarreta, maisbaratocarreta, maisbaratomoto, maisbaratocarro, maisbaratovan;
    private ConstraintLayout van, carro, moto, carreta;
    private EditText peso, distancia, tempo, nomeDestino;
    private DatePickerDialog.OnDateSetListener mDataEntrega;


    AtarefarControler controler = new AtarefarControler();
    Van objectvan = new Van();
    Moto objectmoto = new Moto();
    Car objectcar = new Car();
    Cart objectcart = new Cart();
    User user = new User();



    public AtarefarFragment() {
        // Required empty public constructor
    }






    public static AtarefarFragment newInstance(String param1, String param2) {

        AtarefarFragment fragment = new AtarefarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }






    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



        View view = inflater.inflate(R.layout.fragment_atarefar, container, false);


        van = view.findViewById(R.id.van);
        moto = view.findViewById(R.id.moto);
        peso = view.findViewById(R.id.peso);
        tempo = view.findViewById(R.id.tempo);
        carro = view.findViewById(R.id.carro);
        carreta = view.findViewById(R.id.carreta);
        userName = view.findViewById(R.id.username);
        distancia = view.findViewById(R.id.distancia);
        nomeDestino = view.findViewById(R.id.nomedestino);
        maisrapidocarro = view.findViewById(R.id.textView6);
        maisrapidocarreta = view.findViewById(R.id.textView14);
        maisbaratocarreta = view.findViewById(R.id.textView15);
        maisbaratomoto = view.findViewById(R.id.textView13);
        maisrapidovan = view.findViewById(R.id.textView9);
        maisbaratocarro = view.findViewById(R.id.textView8);
        maisrapidomoto = view.findViewById(R.id.textView12);
        maisbaratovan = view.findViewById(R.id.textView11);


        controler.updateDataBaseName(userName);


        peso.addTextChangedListener(new TextVerify(van, moto, carreta, carro, controler, objectcar, objectvan, objectcart, objectmoto, this));

        distancia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                maisrapidocarreta.setText("- - - -");
                maisrapidocarreta.setText("- - - -");
                maisrapidocarreta.setText("- - - -");
                maisrapidocarreta.setText("- - - -");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String valdistancia = distancia.getText().toString();
                final String valpeso = peso.getText().toString();
                final DecimalFormat formatador = new DecimalFormat("0.00");


                String maisCart =  formatador.format(Float.valueOf(valdistancia)/objectcart.getAverageSpeed());
                String maisCar = formatador.format(Float.valueOf(valdistancia)/objectcar.getAverageSpeed());
                String maisMoto = formatador.format(Float.valueOf(valdistancia)/objectmoto.getAverageSpeed());
                String maisVan = formatador.format(Float.valueOf(valdistancia)/objectvan.getAverageSpeed());


                String menosCart = formatador.format((objectcart.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0));
                String menosCar = formatador.format((objectcar.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0));
                String menosVan = formatador.format((objectvan.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0));
                String menosMoto = formatador.format((objectmoto.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0));


                maisrapidocarreta.setText(maisCart + " horas");
                maisrapidocarro.setText(maisCar + " horas");
                maisrapidomoto.setText(maisMoto + " horas");
                maisrapidovan.setText(maisVan + " horas");
                maisbaratocarreta.setText("R$ " + menosCart);
                maisbaratocarro.setText("R$ " + menosCar);
                maisbaratovan.setText("R$ " + menosVan);
                maisbaratomoto.setText("R$ " + menosMoto);
            }

            @Override
            public void afterTextChanged(Editable s) {
                final String valdistancia = distancia.getText().toString();
                final String valpeso = peso.getText().toString();
                final DecimalFormat formatador = new DecimalFormat("0.00");


                String maisCart =  formatador.format(Float.valueOf(valdistancia)/objectcart.getAverageSpeed());
                String maisCar = formatador.format(Float.valueOf(valdistancia)/objectcar.getAverageSpeed());
                String maisMoto = formatador.format(Float.valueOf(valdistancia)/objectmoto.getAverageSpeed());
                String maisVan = formatador.format(Float.valueOf(valdistancia)/objectvan.getAverageSpeed());


                String menosCart = formatador.format((objectcart.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0));
                String menosCar = formatador.format((objectcar.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0));
                String menosVan = formatador.format((objectvan.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0));
                String menosMoto = formatador.format((objectmoto.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0));


                maisrapidocarreta.setText(maisCart + " horas");
                maisrapidocarro.setText(maisCar + " horas");
                maisrapidomoto.setText(maisMoto + " horas");
                maisrapidovan.setText(maisVan + " horas");
                maisbaratocarreta.setText("R$ " + menosCart);
                maisbaratocarro.setText("R$ " + menosCar);
                maisbaratovan.setText("R$ " + menosVan);
                maisbaratomoto.setText("R$ " + menosMoto);
            }
        });

        return view;

    }






    public void adjustInterface(ConstraintLayout veiculo, final String vehicle) {

        veiculo.setBackgroundResource(R.drawable.shape_background_dark);

        veiculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String valpeso = peso.getText().toString();
                String valdistancia = distancia.getText().toString();
                String valtempo = tempo.getText().toString();
                String nomedestino = nomeDestino.getText().toString();






                if( nomedestino.isEmpty() || valtempo.isEmpty() || valpeso.isEmpty() || valdistancia.isEmpty() ) {

                    showMessage("Please Verify all fields");

                }
                else{
                    Map valoresViagem = new HashMap();
                    valoresViagem.put("nome", nomedestino);
                    valoresViagem.put("peso", valpeso);
                    valoresViagem.put("distancia", valdistancia);
                    valoresViagem.put("tempo", valtempo);
                    Map valoresCusto = new HashMap();
                    if (vehicle.equals("Van")){
                        valoresCusto.put("Custo", ((objectvan.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))));
                        valoresCusto.put("Ganho", ((objectvan.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*(user.getPorcetagem()/100.0)));
                        valoresCusto.put("PreçoTotal", ((objectvan.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0)));
                        valoresCusto.put("veiculo", vehicle);
                        valoresCusto.put("nome", nomedestino);

                    }
                    if (vehicle.equals("Carro")){
                        valoresCusto.put("Custo", ((objectcar.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))));
                        valoresCusto.put("Ganho", ((objectcar.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*(user.getPorcetagem()/100.0)));
                        valoresCusto.put("PreçoTotal", ((objectcar.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0)));
                        valoresCusto.put("veiculo", vehicle);
                        valoresCusto.put("nome", nomedestino);
                    }
                    if (vehicle.equals("Carreta")){
                        valoresCusto.put("Custo", ((objectcart.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))));
                        valoresCusto.put("Ganho", ((objectcart.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*(user.getPorcetagem()/100.0)));
                        valoresCusto.put("PreçoTotal", ((objectcart.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0)));
                        valoresCusto.put("veiculo", vehicle);
                        valoresCusto.put("nome", nomedestino);
                    }
                    if (vehicle.equals("Moto")){
                        valoresCusto.put("Custo", ((objectmoto.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))));
                        valoresCusto.put("Ganho", ((objectmoto.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*(user.getPorcetagem()/100.0)));
                        valoresCusto.put("PreçoTotal", ((objectmoto.LeastCost(Float.valueOf(valdistancia), Float.valueOf(valpeso)))*1+(user.getPorcetagem()/100.0)));
                        valoresCusto.put("veiculo", vehicle);
                        valoresCusto.put("nome", nomedestino);
                    }

                    controler.addOnDataBase(vehicle, nomedestino, valoresViagem, valoresCusto);

                    showMessage("Registered trip");

                    Intent gotToHistorico = new Intent(getActivity(), MainActivity.class);
                    startActivity(gotToHistorico);
                    getActivity().finish();

                }
            }
        });

    }




    public void negAction(ConstraintLayout vehicle) {

        vehicle.setBackgroundResource(R.drawable.shape_background_second);

        vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage("This vehicle is not possible");
            }
        });

    }





    private void showMessage(String message) {

        Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

    }
}
