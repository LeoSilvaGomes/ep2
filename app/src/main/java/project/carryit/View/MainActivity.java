package project.carryit.View;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.TextView;

import project.carryit.R;
import project.carryit.View.AtarefarFragment;
import project.carryit.View.DespesasFragment;
import project.carryit.View.HistoricoFragment;
import project.carryit.View.PerfilFragment;

public class MainActivity extends AppCompatActivity{
    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment selectedFragment = null;

            selectFragment(item, selectedFragment);

            return true;
        }
    };


    public boolean selectFragment(MenuItem item, Fragment selectedFragment) {

        switch (item.getItemId()) {
            case R.id.navigation_perfil:
                selectedFragment = new PerfilFragment();
                break;
            case R.id.navigation_historic:
                selectedFragment = new HistoricoFragment();
                break;
            case R.id.navigation_despesas:
                selectedFragment = new DespesasFragment();
                break;
            case R.id.navigation_atarefar:
                selectedFragment = new AtarefarFragment();
                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, new AtarefarFragment()).commit();



    }
}
