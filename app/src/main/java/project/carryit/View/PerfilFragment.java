package project.carryit.View;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import project.carryit.Models.Car;
import project.carryit.Models.Cart;
import project.carryit.Models.Moto;
import project.carryit.Models.User;
import project.carryit.Models.Van;
import project.carryit.R;

import static android.app.Activity.RESULT_OK;

public class PerfilFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private TextView signout, savetext;
    private TextView name, email, lucro, caminhao, van, moto, carro;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String packed;
    private ImageView profilephoto;
    static int PReqCode = 1;
    static int REQUESCODE = 1;
    Uri pickedImgUri;
    Van objectvan = new Van();
    Moto objectmoto = new Moto();
    Car objectcar = new Car();
    Cart objectcart = new Cart();
    User user = new User();


    public PerfilFragment() {
        // Required empty public constructor
    }

    public static PerfilFragment newInstance(String param1, String param2) {
        PerfilFragment fragment = new PerfilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mAuth = FirebaseAuth.getInstance();



        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("AUTH", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "LogOut", Toast.LENGTH_LONG).show();
                    Log.d("AUTH", "onAuthStateChanged:signed_out");
                    Intent intent = new Intent(getActivity(), RegisterActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }

            }
        };




    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private Task<Void> mDataBase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_perfil, container, false);
        signout = view.findViewById(R.id.logouttext);
        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
            }
        });


        savetext = view.findViewById(R.id.salvetext);
        name = view.findViewById(R.id.textNome);
        email = view.findViewById(R.id.textEmail);
        lucro = view.findViewById(R.id.porclucro);
        caminhao = view.findViewById(R.id.Qcam);
        carro = view.findViewById(R.id.Qcar);
        moto = view.findViewById(R.id.Qmot);
        van = view.findViewById(R.id.Qva);

        savetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id = mAuth.getCurrentUser().getUid();


                if (caminhao.getText().toString().equals("")) {
                    System.out.println(objectcart.getQuantidade());
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("Carreta").child("quantidade").setValue(objectcart.getQuantidade());
                }
                else if (objectcart.getQuantidadealugada() < Integer.valueOf(caminhao.getText().toString())){
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("Carreta").child("quantidade").setValue(Integer.valueOf(caminhao.getText().toString()));
                    objectcart.setQuantidade(Integer.valueOf(caminhao.getText().toString()));
                }
                else{

                }



                if (carro.getText().toString().equals("")) {
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("Carro").child("quantidade").setValue(objectcar.getQuantidade());

                }
                else if (objectcar.getQuantidadealugada() < Integer.valueOf(carro.getText().toString())) {
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("Carro").child("quantidade").setValue(Integer.valueOf(carro.getText().toString()));
                }else{

                }



                if (van.getText().toString().equals("")) {
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("Van").child("quantidade").setValue(objectvan.getQuantidade());

                }
                else if (objectvan.getQuantidadealugada() < Integer.valueOf(van.getText().toString())) {
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("Van").child("quantidade").setValue(Integer.valueOf(van.getText().toString()));
                }
                else{

                }



                if (moto.getText().toString().equals("")) {
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("Moto").child("quantidade").setValue(objectmoto.getQuantidade());

                }
                else if (objectmoto.getQuantidadealugada() < Integer.valueOf(moto.getText().toString())) {
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("Moto").child("quantidade").setValue(Integer.valueOf(moto.getText().toString()));
                }
                else{

                }


                if(lucro.getText().toString().equals("")){

                }
                else{
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("margem_de_lucro").setValue(lucro.getText().toString());
                }
                if(name.getText().toString().equals("")){

                }
                else {
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("name").setValue(name.getText().toString());
                }
                if(email.getText().toString().equals("")){

                }
                else {
                    mDataBase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("email").setValue(email.getText().toString());
                }

                Intent gotToHistorico = new Intent(getActivity(), MainActivity.class);
                startActivity(gotToHistorico);
                getActivity().finish();

            }
        });


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == REQUESCODE && data != null) {

            pickedImgUri = data.getData();
            profilephoto.setImageURI(pickedImgUri);

        }

    }
}
