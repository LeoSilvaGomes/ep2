package project.carryit.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import project.carryit.Controler.LoginUserControler;
import project.carryit.R;

public class LoginActivity extends AppCompatActivity {

    private EditText userMail, userPassword;
    private Button logBtn;
    private ProgressBar loginProgress;
    private Intent gotomainactivity;
    private FirebaseAuth mAuth;

    LoginUserControler loginUserControler = new LoginUserControler(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        userMail = findViewById(R.id.maillogin);
        userPassword = findViewById(R.id.passwordlogin);
        logBtn = findViewById(R.id.logBtn);
        loginProgress = findViewById(R.id.loginprogressBar);
        gotomainactivity = new Intent(this, MainActivity.class);
        mAuth = FirebaseAuth.getInstance();


        loginProgress.setVisibility(View.INVISIBLE);




        logBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginProgress.setVisibility(View.VISIBLE);
                logBtn.setVisibility(View.INVISIBLE);

                final String mail = userMail.getText().toString();
                final String password = userPassword.getText().toString();

                if (mail.isEmpty() || password.isEmpty()){
                    showMessage("Please Verify all fields");
                }
                else{
                    loginUserControler.signIn(mail, password);
                }

            }
        });

    }


    public void VerifySignIn(String signInInformation){

        if (signInInformation == "SignIn Accept"){

            showMessage(signInInformation);
            loginProgress.setVisibility(View.VISIBLE);
            logBtn.setVisibility(View.INVISIBLE);
            updateUI();

        }
        else{

            showMessage(signInInformation);

        }

    }



    private void updateUI() {

        startActivity(gotomainactivity);
        finish();

    }

    private void showMessage(String message) {

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser user = mAuth.getCurrentUser();

        if (user != null){
            // user is already connected so we need to redirect him to home page
            updateUI();
        }

    }
}
