package project.carryit.View;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import project.carryit.Models.Despesas;
import project.carryit.Models.Van;
import project.carryit.R;

class DespesasAdapter extends RecyclerView.Adapter<DespesasAdapter.DespesasHolder> {

    ArrayList<Despesas> despesasArrayList;

    public DespesasAdapter(ArrayList<Despesas> despesasArrayList){
        this.despesasArrayList = despesasArrayList;
    }

    public class DespesasHolder extends RecyclerView.ViewHolder {

        public TextView mTextView1;
        public TextView mTextView2;
        public TextView mTextView3;
        public TextView mTextView4;
        public TextView mTextView5;

        public DespesasHolder(@NonNull View itemView) {
            super(itemView);
            mTextView1 = itemView.findViewById(R.id.destino);
            mTextView2 = itemView.findViewById(R.id.tempoveiculo);
            mTextView3 = itemView.findViewById(R.id.pesocarga);
            mTextView4 = itemView.findViewById(R.id.distancialocal);
            mTextView5 = itemView.findViewById(R.id.veiculo);
        }
    }


    @NonNull
    @Override
    public DespesasHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_custo, viewGroup, false);
        DespesasAdapter.DespesasHolder evh = new DespesasAdapter.DespesasHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull DespesasHolder despesasHolder, int i) {

        Despesas currentItem = despesasArrayList.get(i);

        DecimalFormat formatador = new DecimalFormat("0.000");

        String total = formatador.format(Float.valueOf(currentItem.getText3()));
        String ganho = formatador.format(Float.valueOf(currentItem.getText4()));
        String preço = formatador.format(Float.valueOf(currentItem.getText5()));


        despesasHolder.mTextView1.setText(currentItem.getText1());
        despesasHolder.mTextView2.setText(total);
        despesasHolder.mTextView3.setText(ganho);
        despesasHolder.mTextView4.setText(preço);
        despesasHolder.mTextView5.setText("- " + currentItem.getText2());

    }

    @Override
    public int getItemCount() {
        return despesasArrayList.size();
    }


}
