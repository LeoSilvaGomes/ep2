package project.carryit.View;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import project.carryit.Controler.CreateUserControler;
import project.carryit.R;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    // Variable declaration
    private EditText useremail, userpassword, userpassword2, username;
    private TextView signin;
    private ProgressBar loadingProgress;
    private Button regBtn;




    CreateUserControler createUserControler = new CreateUserControler();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //ini views
        useremail = findViewById(R.id.mail);
        userpassword = findViewById(R.id.password);
        userpassword2 = findViewById(R.id.password2);
        username = findViewById(R.id.name);
        loadingProgress = findViewById(R.id.regprogressBar);
        regBtn = findViewById(R.id.regbtn);
        signin = findViewById(R.id.textView2);

        loadingProgress.setVisibility(View.INVISIBLE);

        mAuth = FirebaseAuth.getInstance();




        regBtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                regBtn.setVisibility(View.INVISIBLE);
                loadingProgress.setVisibility(View.VISIBLE);
                final String email = useremail.getText().toString();
                final String password = userpassword.getText().toString();
                final String password2 = userpassword2.getText().toString();
                final String name = username.getText().toString();

                if( email.isEmpty() || name.isEmpty() || password.isEmpty() || password2.isEmpty() || !password.equals(password2) ){

                    //something goes wrong : all fields  must be filled
                    // we need to displayan error message
                    showMessage("Please Verify all fields");
                    regBtn.setVisibility(View.VISIBLE);
                    loadingProgress.setVisibility(View.INVISIBLE);

                }
                else {

                    // everything is ok  and all fields are filled now we can start creating  user account
                    // CreateUserAccount method will try to create the user  if the email is valid
                    CreateUserAccount(email, name, password);
                }

            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SingInUser();

            }
        });
    }






    private void SingInUser() {

        Intent gotosignin = new Intent(this, LoginActivity.class);
        startActivity(gotosignin);
        finish();

    }





    private void updateUI() {

        Intent goToMainActivity = new Intent( this, MainActivity.class);
        startActivity(goToMainActivity);
        finish();

    }






    private void showMessage(String message) {

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

    }






    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingl
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null){
            // user is already connected so we need to redirect him to home page
            updateUI();
        }
    }




    public void CreateUserAccount(final String email, final String name, String password) {


        // this method create user account with specific email and password
        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){

                            // after we created user account we need to update his info
                            showMessage("Account created");
                            createUserControler.updateUserInfo(email, name, mAuth.getCurrentUser());
                            updateUI();

                        }
                        else{

                            showMessage("Failed register account");
                            regBtn.setVisibility(View.VISIBLE);
                            loadingProgress.setVisibility(View.INVISIBLE);

                        }
                    }
                });

    }



}

