package project.carryit.View;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import project.carryit.Models.Despesas;
import project.carryit.Models.Historico;
import project.carryit.Models.Van;
import project.carryit.R;

class HistoricoAdapter extends RecyclerView.Adapter<HistoricoAdapter.HistoricoHolder> {

    ArrayList<Historico> historicoArrayList;

    public HistoricoAdapter (ArrayList<Historico> historicoArrayList){
        this.historicoArrayList = historicoArrayList;
    }

    public class HistoricoHolder extends RecyclerView.ViewHolder {

        public TextView mTextView1;
        public TextView mTextView2;
        public TextView mTextView3;
        public TextView mTextView4;
        public TextView mTextView5;

        public HistoricoHolder(@NonNull View itemView) {
            super(itemView);

            mTextView1 = itemView.findViewById(R.id.destino);
            mTextView2 = itemView.findViewById(R.id.tempoveiculo);
            mTextView3 = itemView.findViewById(R.id.pesocarga);
            mTextView4 = itemView.findViewById(R.id.distancialocal);
            mTextView5 = itemView.findViewById(R.id.veiculo);

        }
    }


    @NonNull
    @Override
    public HistoricoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_historico    , viewGroup, false);
        HistoricoAdapter.HistoricoHolder evh = new HistoricoAdapter.HistoricoHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoricoHolder historicoHolder, int i) {
        Historico currentItem = historicoArrayList.get(i);

        historicoHolder.mTextView1.setText(currentItem.getText1());
        historicoHolder.mTextView2.setText(currentItem.getText3());
        historicoHolder.mTextView3.setText(currentItem.getText4());
        historicoHolder.mTextView4.setText(currentItem.getText5());
        historicoHolder.mTextView5.setText("- " + currentItem.getText2());
    }

    @Override
    public int getItemCount() {
        return historicoArrayList.size();
    }

}
